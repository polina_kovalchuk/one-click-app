export const fetchMockActions = () => Promise.resolve({ data: getActionsMock });

const getActionsMock = {
  "TrackingAction": [
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "actionId": "094D4D25-5AB0-951A-5B24-E0BC118DE15A",
      "activityId": "4D11AC9E-9909-0DA3-681E-36C8898C1490",
      "name": "CreateAI",
      "description": "Create Workspaces with apporpiate Applets enabled",
      "status": "SUCCESS",
      "errorDescription": ""
    },
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "actionId": "2ECFDD6C-4A5B-0025-A052-FBA4D7D7972E",
      "activityId": "4D11AC9E-9909-0DA3-681E-36C8898C1490",
      "name": "ConfigureAIGids",
      "description": "Configure Default WorkitemTypes setting for ADE validation",
      "status": "SUCCESS",
      "errorDescription": ""
    },
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "actionId": "4AAB77CC-FC97-64EF-2D24-77AC2867529E",
      "activityId": "4D11AC9E-9909-0DA3-681E-36C8898C1490",
      "name": "EnableApplications",
      "description": "Enable Orchestration Integration Applications",
      "status": "SUCCESS",
      "errorDescription": ""
    },
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "actionId": "7A2009A8-0663-D473-F5FE-48A4B81E5B3D",
      "activityId": "4D11AC9E-9909-0DA3-681E-36C8898C1490",
      "name": "CreateOfficeAndCabinet",
      "description": "Default Office Cabinet used for Integration",
      "status": "SUCCESS",
      "errorDescription": ""
    },
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "actionId": "86CD9E82-7119-E6F4-0586-998A6774B338",
      "activityId": "4D11AC9E-9909-0DA3-681E-36C8898C1490",
      "name": "CreateExtractionSets",
      "description": "Custom Extraction Set with Supported DocType",
      "status": "SUCCESS",
      "errorDescription": ""
    },
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "actionId": "A209FA93-5358-FDC5-8F1C-3E99402172DC",
      "activityId": "4D11AC9E-9909-0DA3-681E-36C8898C1490",
      "name": "CreateWorkflows",
      "description": "Create FileIntake Workflows with ADR and ADE Validation steps",
      "status": "SUCCESS",
      "errorDescription": ""
    },
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "actionId": "A4A90BD1-8B50-AFA3-A69B-EFC9961ED539",
      "activityId": "4D11AC9E-9909-0DA3-681E-36C8898C1490",
      "name": "DataMapperConfig",
      "description": "Configure DataMapper DefaultConfig with Supported DocType and Properties",
      "status": "SUCCESS",
      "errorDescription": ""
    },
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "actionId": "ABFF5496-6580-7D31-1945-FED450A2D49B",
      "activityId": "4D11AC9E-9909-0DA3-681E-36C8898C1490",
      "name": "UpdateDocumentDictionary",
      "description": "Update Document Dictionary for Supported DocType changes",
      "status": "SUCCESS",
      "errorDescription": ""
    },
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "actionId": "F73AE4C5-7C3C-1E1B-15B8-3E973B81CB15",
      "activityId": "4D11AC9E-9909-0DA3-681E-36C8898C1490",
      "name": "CreateRoles",
      "description": "Create Default Roles with Default Privileges",
      "status": "SUCCESS",
      "errorDescription": ""
    },
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "actionId": "FBA509BD-2A92-B9FE-B0AE-B6D06DD00C17",
      "activityId": "4D11AC9E-9909-0DA3-681E-36C8898C1490",
      "name": "ConfigureWorkitemType",
      "description": "Configure Default WorkitemTypes setting for ADE validation",
      "status": "SUCCESS",
      "errorDescription": ""
    }
  ]
};
