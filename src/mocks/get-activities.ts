export const fetchMockActivities = () => Promise.resolve({ data: getActivitiesMock });

const getActivitiesMock = {
  "TrackingActivity": [
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "activityId": "4D11AC9E-9909-0DA3-681E-36C8898C1490",
      "name": "AIQAdmin",
      "description": "",
      "status": "FAILURE",
      "errorDescription": "Something went wrong while processing activity"
    },
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "activityId": "7A154CD5-98E5-DEA8-F0DC-ED04EEFE9733",
      "name": "DMS",
      "status": "FAILURE",
      "errorDescription": "Something went wrong while processing activity"
    },
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "activityId": "BD19CF1A-684B-1E6C-5C2C-B2768F834E34",
      "name": "AIQSRE",
      "description": "",
      "status": "FAILURE",
      "errorDescription": "Something went wrong while processing activity"
    },
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "activityId": "F2240BFD-3720-AE04-AB36-C0C692202D7E",
      "name": "EncompassSRE",
      "description": "",
      "status": "FAILURE",
      "errorDescription": "Something went wrong while processing activity"
    },
    {
      "eventId": "CONFIG_EFCD6D56-9906-DA4C-5ED0-701C59232378",
      "activityId": "FC72C943-287A-31D3-3EA5-46C31FB1B9F4",
      "name": "AIQSync",
      "description": "",
      "status": "FAILURE",
      "errorDescription": "Something went wrong while processing activity"
    }
  ]
};
