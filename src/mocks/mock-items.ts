export const stepItems = [
  {
    "name": "Add applications",
    "status": 2,
    "description": `Add and set application password for:
    
    capsilon-mpos
    app-lis
    integration framework`
  },
  {
    "name": "Master document type",
    "status": 3,
    "description": `Paystub
    IRS W-2
    Verification of Employment
    Electronic Verification of Employment
    FHA Case Number Assignments
    Mortgage Statement`
  },
  {
    "name": "Create workspaces",
    "status": 2,
    "description": `Paystub
    IRS W-2
    Verification of Employment
    Electronic Verification of Employment
    FHA Case Number Assignments
    Mortgage Statement`
  },
  {
    "name": "Enable Applicant Association",
    "status": 2,
    "description": `Paystub
    IRS W-2
    Verification of Employment
    Electronic Verification of Employment
    FHA Case Number Assignments
    Mortgage Statement`
  },
  {
    "name": "Master document type1",
    "status": 3,
    "description": `Paystub
    IRS W-2
    Verification of Employment
    Electronic Verification of Employment
    FHA Case Number Assignments
    Mortgage Statement`
  },
  {
    "name": "Master document type2",
    "status": 3,
    "description": `Paystub
    IRS W-2
    Verification of Employment
    Electronic Verification of Employment
    FHA Case Number Assignments
    Mortgage Statement`
  }
];
