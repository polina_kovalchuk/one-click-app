import React, { useEffect } from 'react';
import {
  RerunButton,
  Step as Container,
  StepActionContainer as ActionContainer,
  ItemsList,
  StepItem,
} from '../components';
import { StepHeader } from '../components/Step/StepHeader';
import { useDispatch, useSelector } from 'react-redux';
import { getActions } from '../state/selectors';
import { fetchActions } from '../state/thunks';
import { useParams } from 'react-router';

interface StepProps {
  name: string;
  site: string;
}

export const Step: React.FC<StepProps> = ({ site }) => {
  const items = useSelector(getActions);
  const dispatch = useDispatch();
  const { name } = useParams()

  useEffect(() => {
    dispatch(fetchActions(name || ''));
  }, [dispatch, name]);

  const rerunItem = () => {};

  return (
    <Container>
      <StepHeader name={name || ''} site={site} />
      <ActionContainer>
        <RerunButton onClick={() => {}}>
          Rerun failed steps
        </RerunButton>
      </ActionContainer>
      <ItemsList>
        {items.map((item) => (
          <StepItem item={item} key={item.id} rerun={rerunItem}/>
        ))}
      </ItemsList>
    </Container>
  );
};
