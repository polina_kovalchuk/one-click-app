import React, { useCallback, useEffect } from 'react';
import {
  Chain,
  Main as Container,
  StartButton,
  StopButton,
  Progress,
  MainActionContainer as ActionContainer,
  MainSelectContainer as SelectContainer,
} from '../components';
import { EncompassSelector } from './EncompassSelector';
import { SiteSelector } from './SiteSelector';
import { MainTitle } from '../components/Main/MainTitle';
import { useDispatch, useSelector } from 'react-redux';
import { fetchActivities, runConfiguration } from '../state/thunks';
import {
  getActivities,
  getEncompassId,
  getEventIdFromHistory,
  getIsRunning,
  getSiteName,
  isConfigurationCompleted
} from '../state/selectors';
import { stopConfiguration } from '../state/actions';
import { FinishMessage } from '../components/FinishMessages';

// const mockItems = [
//   { status: Status.SUCCESSFUL, name: 'AIQ Operations'},
//   { status: Status.SUCCESSFUL, name: 'Site Admin'},
//   { status: Status.FAILED, name: 'RPA Rules'},
//   { status: Status.IN_PROGRESS, name: 'DMS Operations'},
//   { status: Status.FAILED, name: 'LIS3 Operations'},
//   { status: Status.EMPTY, name: 'LIS3 Configuration'},
// ];

export const Main = () => {
  const isRunning = useSelector(getIsRunning);
  const items = useSelector(getActivities);
  const siteName = useSelector(getSiteName);
  const encompassId = useSelector(getEncompassId);
  const dispatch = useDispatch();
  const eventId = useSelector(getEventIdFromHistory);
  const isFinished = useSelector(isConfigurationCompleted);

  const fetch = useCallback(() =>
    dispatch(fetchActivities()),
    [dispatch]);

  useEffect(() => {
    fetch();
  }, [fetch, isRunning, siteName, encompassId]);

  const onStartClick = () => dispatch(runConfiguration());

  const onStopClick = () => {
    dispatch(stopConfiguration());
  }

  const getAction = () => {
    if (eventId) {
      if (isFinished) {
        return (
          <FinishMessage siteName={siteName}>
            Configuration is completed.
          </FinishMessage>
        )
      }

      return <><StopButton onClick={onStopClick} /><Progress refresh={fetch}/></>;
    }

    return <StartButton onClick={onStartClick} />;
  }

  return (
    <Container>
      <MainTitle />
      <SelectContainer>
        <EncompassSelector />
        <SiteSelector />
      </SelectContainer>
      <ActionContainer>
        {getAction()}
      </ActionContainer>
      <div>
        <Chain items={items}/>
      </div>
    </Container>
  );
}
