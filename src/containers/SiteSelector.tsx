import React from 'react';
import { Select } from '../components';
import { useSelectInput } from '../utils';
import { setSiteName } from '../state/actions';
import { getSiteName } from '../state/selectors';
import { addToStorage, getFromStorage } from '../utils/storage';

let options = [
  'oneclick-demo1.acceptance1-capsilondev.net',
  'oneclick-demo2.acceptance1-capsilondev.net',
  'oneclick-demo3.acceptance1-capsilondev.net',
  'oneclick-demo4.acceptance1-capsilondev.net',
  ...(getFromStorage('sites') || []),
];

export const SiteSelector: React.FC = () => {
  const { selected, onSelect } = useSelectInput(options, setSiteName, getSiteName);

  const addOption = (value: string) => {
    options = [...options, value];
    addToStorage('sites', value);
    onSelect(value);
  };

  return (
    <Select
      addOption={addOption}
      label="Site Name"
      value={selected}
      options={options}
      selected={selected}
      select={onSelect}
      width={620}
    />
  );
};
