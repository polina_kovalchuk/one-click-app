import React from 'react';
import { Select } from '../components';
import { useSelectInput } from '../utils';
import { setEncompassInstance } from '../state/actions';
import { getEncompassId } from '../state/selectors';

let options = [
  'BE11158783',
  'BE11214846'
];

export const EncompassSelector: React.FC = () => {
  const addOption = (value: string) => {
    options = [...options, value];
    onSelect(value);
  };

  const { selected, onSelect } = useSelectInput(
    options,
    setEncompassInstance,
    getEncompassId
  );

  return (
    <Select
      addOption={addOption}
      label="Encompass Instance"
      value={selected}
      options={options}
      selected={selected}
      select={onSelect}
    />
  );
};
