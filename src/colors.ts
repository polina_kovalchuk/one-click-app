const colors = {
  backgroundNeutral: '#C5DBEC',
  backgroundStep: '#D2D9C4',
};

export default colors;
