import React from 'react';
import './App.css';
import { Step, Main } from './containers';
import { store } from './state/store';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <BrowserRouter basename="/oneclick">
          <Routes>
            <Route path="/" element={<Main />} />
            <Route path="/activity/:name" element={<Step name="Site Admin" site="qa-mdms.integration2.capsilondev.net"/>} />
          </Routes>
        </BrowserRouter>
      </div>
    </Provider>
  );
}

export default App;
