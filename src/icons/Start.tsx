import React from 'react';

export const StartIcon = () => (
  <svg width="29" height="30" viewBox="0 0 29 30" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M27.0905 13.9159C28.4826 14.6866 28.4609 16.6955 27.0525 17.4359L3.77714 29.6732C2.43694 30.3778 0.830195 29.3954 0.846534 27.8813L1.11604 2.90743C1.13237 1.39338 2.75995 0.445886 4.08463 1.17926L27.0905 13.9159Z" fill="#FCF7F7"/>
  </svg>
);
