import React from 'react';
import { IconType } from './types';

export const TriangleIcon: React.FC<IconType> = ({className, onClick}) => (
  <svg {...{className, onClick}} width="13" height="12" viewBox="0 0 13 12" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M6.15452 11.2588L0.193817 0.546263L12.5858 0.821969L6.15452 11.2588Z" fill="white"/>
  </svg>
);

