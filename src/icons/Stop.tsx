import React from 'react';

export const Stop = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="26" height="25" viewBox="0 0 26 25" fill="none">
    <rect width="26" height="25" rx="4" fill="#FFF5F5"/>
  </svg>
);
