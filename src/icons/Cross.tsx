import React from 'react';

export const CrossIcon = () => (
  <svg width="30" height="32" viewBox="0 0 30 32" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M28 30L15 16M2 2L15 16M15 16L28 2L2 30" stroke="#FEFBFB" strokeWidth="4"/>
  </svg>
);
