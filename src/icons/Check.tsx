import React from 'react';

export const CheckIcon = () => (
  <svg width="26" height="29" viewBox="0 0 26 29" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M2 10.8824L12 25L24 1" stroke="#FEFBFB" strokeWidth="4"/>
  </svg>
);
