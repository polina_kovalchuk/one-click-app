export interface IconType {
  className?: string;
  onClick?: () => void;
}