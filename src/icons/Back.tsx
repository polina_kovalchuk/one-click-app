import React from 'react';
import { IconType } from './types';

export const BackIcon: React.FC<IconType> = ({ className }) => (
  <svg {...{className}} width="20" height="30" viewBox="0 0 20 30" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g filter="url(#filter0_d_34_6)">
      <path d="M14.8197 21L6.34482 12.8771C5.08101 11.6658 5.11853 9.63413 6.4262 8.47029L14.8197 1" stroke="white" strokeWidth="2"/>
    </g>
    <defs>
      <filter id="filter0_d_34_6" x="0.420715" y="0.253021" width="19.0909" height="29.4689" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
        <feFlood floodOpacity="0" result="BackgroundImageFix"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
        <feOffset dy="4"/>
        <feGaussianBlur stdDeviation="2"/>
        <feComposite in2="hardAlpha" operator="out"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_34_6"/>
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_34_6" result="shape"/>
      </filter>
    </defs>
  </svg>
);
