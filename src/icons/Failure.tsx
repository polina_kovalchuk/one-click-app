import React from 'react';

export const FailureIcon = () => (
  <svg width="43" height="42" viewBox="0 0 43 42" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g filter="url(#filter0_d_35_53)">
      <ellipse cx="21.5" cy="17" rx="17.5" ry="17" fill="#EE9898"/>
      <path d="M37.5 17C37.5 25.5197 30.3778 32.5 21.5 32.5C12.6222 32.5 5.5 25.5197 5.5 17C5.5 8.48028 12.6222 1.5 21.5 1.5C30.3778 1.5 37.5 8.48028 37.5 17Z" stroke="white" strokeWidth="3"/>
    </g>
    <path d="M29.9259 25.9636L21.5 17.3091M13.0741 8.65454L21.5 17.3091M21.5 17.3091L29.9259 8.65454L13.0741 25.9636" stroke="#FEFBFB" strokeWidth="4"/>
    <defs>
      <filter id="filter0_d_35_53" x="0" y="0" width="43" height="42" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
        <feFlood floodOpacity="0" result="BackgroundImageFix"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
        <feOffset dy="4"/>
        <feGaussianBlur stdDeviation="2"/>
        <feComposite in2="hardAlpha" operator="out"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_35_53"/>
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_35_53" result="shape"/>
      </filter>
    </defs>
  </svg>
);
