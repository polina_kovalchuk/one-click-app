import React from 'react';

export const SuccessIcon = () => (
  <svg width="43" height="40" viewBox="0 0 43 40" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g filter="url(#filter0_d_35_49)">
      <ellipse cx="21.5" cy="16" rx="17.5" ry="16" fill="#BECD9E"/>
      <path d="M37.5 16C37.5 23.8844 30.4659 30.5 21.5 30.5C12.5341 30.5 5.5 23.8844 5.5 16C5.5 8.11556 12.5341 1.5 21.5 1.5C30.4659 1.5 37.5 8.11556 37.5 16Z" stroke="white" strokeWidth="3"/>
    </g>
    <path d="M14.3704 13.8952L20.8518 22.1091L28.6296 8.14545" stroke="#FEFBFB" strokeWidth="4"/>
    <defs>
      <filter id="filter0_d_35_49" x="0" y="0" width="43" height="40" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
        <feFlood floodOpacity="0" result="BackgroundImageFix"/>
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
        <feOffset dy="4"/>
        <feGaussianBlur stdDeviation="2"/>
        <feComposite in2="hardAlpha" operator="out"/>
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_35_49"/>
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_35_49" result="shape"/>
      </filter>
    </defs>
  </svg>

);
