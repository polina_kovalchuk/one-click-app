export const getFromStorage = (key: string) => {
  const value = localStorage.getItem(key);
  try {
    return JSON.parse(value || '');
  } catch(e) {
    return null;
  }
};

export const addToStorage = (key: string, value: string) => {
  const current = getFromStorage(key) || [];
  localStorage.setItem(key, JSON.stringify([...current, value]));
};