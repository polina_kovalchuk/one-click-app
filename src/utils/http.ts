import axios from 'axios';

export const httpClient = axios.create({ baseURL: 'https://awh2.acceptance1-capsilondev.net/lis-gateway2/services/v1'});
