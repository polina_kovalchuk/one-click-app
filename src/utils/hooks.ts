import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ActionCreatorWithPayload } from '@reduxjs/toolkit';

export function useSelectInput(
  options: string[],
  action: ActionCreatorWithPayload<string | null>,
  selector: any
) {
  const dispatch = useDispatch();
  const selected = useSelector(selector) as string || options[0];

  useEffect(() => {
    dispatch(action(selected));
    // set selected default option to state, should only be executed once
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onSelect = (value: string) => {
    dispatch(action(value));
  }

  return {
    selected,
    onSelect,
  };
}