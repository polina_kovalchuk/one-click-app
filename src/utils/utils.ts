export const fromCamelCase = (value: string): string => {
  const result = value.replace(/([A-Z][a-z])/g, " $1");
  return result.charAt(0).toUpperCase() + result.slice(1);
};
