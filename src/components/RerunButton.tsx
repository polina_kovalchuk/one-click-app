import React from 'react';
import styled from 'styled-components';
import { RerunIcon } from '../icons';
import { Button as BaseButton } from './Button';

const Button = styled(BaseButton)`
  display: none;
  padding: 8px;
  background: #DC7B7B;
  border-radius: 6px;
  font-family: Roboto Mono;
  font-weight: 500;
  font-size: 20px;
  
  svg {
    margin-right: 15px;
  }
`;

interface ButtonProps {
  onClick: () => void;
}

export const RerunButton: React.FC<ButtonProps> = ({ onClick, children }) => (
  <Button onClick={onClick} >
    <RerunIcon />
    {children}
  </Button>
);

