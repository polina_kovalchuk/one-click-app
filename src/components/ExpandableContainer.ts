import styled from 'styled-components';

export interface ExpandableContainerProps {
  isCollapsed: boolean;
}

export const ExpandableContainer = styled.div<ExpandableContainerProps>`
  overflow-y: hidden;
  // height: ${({ isCollapsed }) => isCollapsed ? '0px' : 'auto'};
  display: ${({ isCollapsed }) => isCollapsed ? 'none' : 'block'};
  box-sizing: border-box;
`;
