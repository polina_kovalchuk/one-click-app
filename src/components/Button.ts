import styled from 'styled-components';

export const Button = styled.div`
  cursor: pointer;
  color: white;
  display: flex;
  align-items: center;
  box-sizing: border-box;
`;
