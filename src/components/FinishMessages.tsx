import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  color: #365b89;
  font-size: 20px;
  font-weight: bold;
  font-family: Roboto Mono;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Link = styled.a`
  margin-top: 15px;
  padding: 10px;
  color: #2057A9;
  font-weight: 200;
  border: 2px solid #6F88B7;
  text-decoration: none;
  border-radius: 6px;
  cursor: pointer;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);

  
  :hover {
    background: #365b8914;
  }
`;

interface FinishMessageProps {
  siteName: string | null;
}

export const FinishMessage: React.FC<FinishMessageProps> = ({ siteName, children }) => {
  return (
    <Container>
      {children}
      {siteName && <Link href={`https://${siteName}`}>{siteName}</Link>}
    </Container>
  );
}
