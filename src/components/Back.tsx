import React from 'react';
import styled from 'styled-components';
import { BackIcon } from '../icons';

interface BackProps {
  onClick?: () => void;
}

const Container = styled.div`
  width: 50px;
  height: 50px;
  border: 2px solid #6B8B60;
  border-radius: 1000px;
  box-sizing: border-box;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 20px;
  padding-top: 4px;
  box-sizing: border-box;
  cursor: pointer;
`;

// const Icon = styled.div`
//   width: 20px;
//   height: 20px;
//   border: 3px solid white;
//   border-right: none;
//   border-bottom: none;
//   border-radius: 4px;
//   transform: rotate(-45deg);
// `;

const Icon = styled(BackIcon)`
  // box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`;

export const Back: React.FC<BackProps> = ({ onClick = () => {} }) => (
  <Container onClick={onClick}>
    <Icon />
  </Container>
);
