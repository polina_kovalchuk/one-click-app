import styled from 'styled-components';
import { Status } from '../state/types';
import React, { ReactNode } from 'react';
import { ExpandableContainer, ExpandableContainerProps } from './ExpandableContainer';

const getBackground = (status?: Status) => {
  switch (status) {
    case Status.FAILED:
      return 'rgba(235, 196, 188, 0.53)';
    case Status.SUCCESSFUL:
    default:
      return 'rgba(229, 240, 232, 0.53)';
  }
}

interface ContainerProps extends ExpandableContainerProps{
  status: Status;
}

const Container = styled(ExpandableContainer)<ContainerProps>`
  border-radius: 3px;
  background: ${({ status }) => getBackground(status)};
  padding: 12px 28px;
  font-family: Roboto Mono;
  font-size: 16px;
  color: #000000;
  position: relative;
  white-space: pre-wrap;
  text-align: start;
  transition: display 2s ease-out;
  margin-left: 66px;
  ${({ status }) => status === Status.SUCCESSFUL ? `width: 500px;`: ''}
`;

const ButtonsContainer = styled.div`
  position: absolute;
  top: 11px;
  right: 18px;
`;

interface ItemDescriptionProps {
  status: Status;
  isCollapsed: boolean;
  buttons: ReactNode[];
}


export const ItemDescription: React.FC<ItemDescriptionProps> = ({ status, isCollapsed, buttons, children }) => (
  <Container status={status} isCollapsed={isCollapsed}>
    {children}
    {status === Status.FAILED && (
      <ButtonsContainer>
        {buttons}
      </ButtonsContainer>
    )}
  </Container>
);
