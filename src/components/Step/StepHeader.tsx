import React from 'react';
import styled from 'styled-components';
import { Back } from '../Back';
import { Link } from 'react-router-dom';

const Site = styled.div`
  font-family: Roboto Mono;
  font-size: 22px;
  color: #324C8F;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
`;

const Name = styled.div`
  font-family: Roboto;
  font-size: 33px;
  color: #6B8B60;
  text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  line-height: 47px;
`;

const NameContainer = styled.div`
  display: flex;
`;

const Container = styled.div`
  display: flex;
  justify-content: space-between;
`;

interface HeaderProps {
  name: string;
  site: string;
}

export const StepHeader: React.FC<HeaderProps> = ({ name, site}) => (
  <Container>
    <NameContainer>
      <Link to="/">
        <Back/>
      </Link>
      <Name>{name}</Name>
    </NameContainer>
    <Site>{site}</Site>
  </Container>
);
