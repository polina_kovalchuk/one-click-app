import React, { useState } from 'react';
import { ItemDescription } from '../ItemDescription';
import { Item } from '../Item';
import { RerunButton } from '../RerunButton';

interface StepItemProps {
  item: any;
  rerun: () => void;
}

export const StepItem: React.FC<StepItemProps> = ({ item, rerun }) => {
  const [isCollapsed, setIsCollapsed] = useState(true);

  const { displayName, status, description } = item;

  const onExpandClick = () => setIsCollapsed(!isCollapsed);

  const buttons = [
    <RerunButton onClick={rerun} key="rerun">Rerun</RerunButton>
  ];

  return (
    <>
      <Item label={displayName} status={status} isCollapsed={isCollapsed} onClick={onExpandClick} />
      {description && <ItemDescription status={status} isCollapsed={isCollapsed} buttons={buttons}>
        {description}
      </ItemDescription>}
    </>
  );
};
