import styled from 'styled-components';

export const StepActionContainer = styled.div`
  display: flex;
  padding: 40px 0;
  width: 100%;
`;