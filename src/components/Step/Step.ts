import styled from 'styled-components';
import colors from '../../colors';
import { Page } from '../Page';

export const Step = styled(Page)`
  background: ${colors.backgroundStep};
  padding: 27px 38px;
`;
