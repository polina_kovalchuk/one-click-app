import styled from 'styled-components';
import { ScrollableContainer } from './ScrollableContainer';

export const ItemsList = styled(ScrollableContainer)`
  width: 100%;
  padding-right: 20px;
`;