import React from 'react';
import { ExpandableContainer, ExpandableContainerProps } from '../ExpandableContainer';
import styled from 'styled-components';
import { Option } from './Option';

const Container = styled(ExpandableContainer)`
  background: rgba(215, 231, 242);
  border: 2px solid #FFFFFF;
  border-radius: 5px;
  width: 100%;
  position: absolute;
  margin-top: 10px;
  z-index: 1;
`;

interface DropdownProps extends ExpandableContainerProps {
  options: any[];
  selectedOption: any;
  onSelect: (option: any) => void;
  addOption: () => void;
}

export const Dropdown: React.FC<DropdownProps> = ({ isCollapsed, options, selectedOption, onSelect, addOption }) => {
  return (
    <Container isCollapsed={isCollapsed}>
      <>
      {options.map((option) => (
        <Option
          key={option}
          onClick={() => onSelect(option)}
          isSelected={option === selectedOption}
        >{option}</Option>
      ))}
      <Option isSelected={false} onClick={addOption}>+ Add new</Option>
      </>
    </Container>
  )

}