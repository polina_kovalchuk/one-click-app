import styled from 'styled-components';

export const Value = styled.span`
  font-size: 22px;
  font-family: Roboto Mono;
  color: #464D8C;
`;