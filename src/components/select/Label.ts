import styled from 'styled-components';
import colors from '../../colors';

export const Label = styled.div`
  color: white;
  font-size: 22px;
  padding: 0px 10px;
  position: absolute;
  top: -16px;
  left: 30px;
  background: ${colors.backgroundNeutral}
`;