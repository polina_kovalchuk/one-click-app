import styled from 'styled-components';
import { Expand as BaseExpand } from '../Expand';

export const Expand = styled(BaseExpand)`
  position: absolute;
  right: 20px; 
`;
