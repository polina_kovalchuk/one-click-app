import React, { useState } from 'react';
import styled from 'styled-components';
import { Label } from './Label';
import { Expand } from './Expand';
import { Value } from './Value';
import { Dropdown } from './Dropdown';

interface ContainerProps {
  width?: number;
}

const Container = styled.div<ContainerProps>`
  position: relative;
  align-self: center;
  min-width: ${({ width }) => width ? width : 300}px;
`;

const Result = styled.div`
  width: 100%;
  height: 50px;
  border: 2.5px solid #FEFBFB;
  border-radius: 6px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 11px 42px 10px 15px;
  box-sizing: border-box;
`;

const Input = styled.input`
  left: 5px;
  width: 100%;
  position: absolute;
  background: transparent;
  border: none;
  font-size: 22px;
  font-family: Roboto Mono;
  color: #464D8C;
  outline: none;
`;

interface SelectProps {
  label: string;
  value: string;
  options: string[];
  selected: string;
  select: (option: string) => void;
  addOption: (value: string) => void;
  width?: number;
}

export const Select: React.FC<SelectProps> = ({ label, value, options, selected, select, addOption, width }) => {
  const [isCollapsed, setIsCollapsed] = useState(true);
  const [isInputMode, setIsInputMode] = useState(false);
  const [inputValue, setInputValue] = useState('');

  const onInputValueChange = (e: any) => {
    setInputValue(e?.target?.value || '');
  }

  const onInputBlur = () => {
    if (inputValue) {
      addOption(inputValue);
    }
    setInputValue('');
    setIsInputMode(false);
  }

  const onEnterPress = (e: any) => {
    if (e.key === 'Enter') {
      onInputBlur();
    }
  }

  const onExpandClick = () => {
    setIsCollapsed(!isCollapsed);
  }

  const onOptionClick = (value: string) => {
    select(value);
    setIsCollapsed(true);
  }

  const onNewOptionClick = () => {
    setIsCollapsed(true);
    setIsInputMode(true);
  }

  return (
    <Container width={width}>
      <Result>
        <Label>{label}</Label>
        {!isInputMode && <Value>{value}</Value>}
        {isInputMode && <Input autoFocus onChange={onInputValueChange} onBlur={onInputBlur} onKeyPress={onEnterPress}/>}
        <Expand isCollapsed={isCollapsed} onClick={onExpandClick}/>
      </Result>
      <Dropdown
        isCollapsed={isCollapsed}
        options={options}
        selectedOption={selected}
        addOption={onNewOptionClick}
        onSelect={onOptionClick}/>
    </Container>
  );
};
