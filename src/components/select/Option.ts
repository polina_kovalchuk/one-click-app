import styled from 'styled-components';
import { Value } from './Value';

interface OptionProps {
  isSelected: boolean;
}

export const Option = styled(Value)<OptionProps>`
  display: block;
  padding: 10px 0;
  cursor: pointer;
  
  &&& {
    color: '#0A0B0A';
  }  
  
  :hover {
    // background: rgba(137, 186, 232, 0.05);
  }
  
  ${({ isSelected }) => isSelected ? 'background: rgba(137, 186, 232, 0.17);' : ''} 
`;