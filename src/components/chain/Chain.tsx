import React from 'react';
import { Line } from './Line';
import { Circle } from './Circle';
import { Container } from './Container';
import { Status } from '../../state/types';
import { Link } from 'react-router-dom';

interface Step {
  status: Status;
  name: string;
}

interface ChainProps {
  items: Step[];
}

export const Chain: React.FC<ChainProps> = ({ items = [] }) => {

  return (
    <Container>
      {items.map((item, index) => (
        <React.Fragment key={item.name}>
          <Link to={`/activity/${item.name}`}>
            <Circle status={item.status} name={item.name} index={index} />
          </Link>
          {index < items.length -1 && <Line/>}
        </React.Fragment>
      ))}
    </Container>
  )
};
