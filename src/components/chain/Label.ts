import styled from 'styled-components';
import { Status } from '../../state/types';

interface LabelProps {
  status?: Status;
  index: number;
}

const getColor = (status?: Status) => {
  switch (status) {
    // case Status.IN_PROGRESS:
    //   return '#F8E270';
    // case Status.SUCCESSFUL:
    //   return '#96AD51';
    // case Status.FAILED:
    //   return '#DD8686'
    case Status.EMPTY:
    default:
      return '#FFFFFF';
  }
}

export const Label = styled.span<LabelProps>`
  font-size: 18px;
  font-weight: bold;
  color: ${({ status }) => getColor(status)};
  position: absolute;
  ${({ index }) => index % 2 === 0 ? 'top: 0;' : 'bottom: 0;' }
  cursor: pointer;
`;

export const LabelContainer = styled.div`
  position: absolute;
  width: 140px;
  height: 150px;
  left: -44px;
  top: -48px;
  display: flex;
  justify-content: center;
`;