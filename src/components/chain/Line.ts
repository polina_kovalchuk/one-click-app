import styled from 'styled-components';

export const Line = styled.div`
  height: 3px;
  width: 76px;
  background: white;
  display: inline-block;
`;