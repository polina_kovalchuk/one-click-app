import React from 'react';
import styled from 'styled-components';
import { Status } from '../../state/types';
import { CheckIcon, CrossIcon, ProgressIcon } from '../../icons';
import { LabelContainer, Label } from './Label';

interface CircleContainerProps {
  status?: Status;
}

const getBackground = (status?: Status) => {
  switch (status) {
    case Status.IN_PROGRESS:
      return '#F8DB91';
    case Status.SUCCESSFUL:
      return '#BECD9E';
    case Status.FAILED:
      return '#EE9898'
    case Status.EMPTY:
    default:
      return 'none';
  }
}

const CircleContainer = styled.div<CircleContainerProps>`
  position: relative;
  width: 55px;
  height: 55px;
  background-color: ${({ status }) => getBackground(status)};
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
  border: 3px solid white;
  display: inline-block;
  margin: 0 auto;
  border-radius: 1000px;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  box-sizing: border-box;
`;

interface CircleProps extends CircleContainerProps {
  index: number;
  name: string;
}

export const Circle: React.FC<CircleProps> = ({ status, name, index }) => {
  return (
    <CircleContainer status={status}>
      <LabelContainer>
        <Label index={index} status={status}>
          {name}
        </Label>
      </LabelContainer>
      {(() => {
        switch (status) {
          case Status.IN_PROGRESS:
            return <ProgressIcon />;
          case Status.SUCCESSFUL:
            return <CheckIcon />;
          case Status.FAILED:
            return <CrossIcon />;
          case Status.EMPTY:
          default:
            return null;
        }
      })()}
    </CircleContainer>
  );
};
