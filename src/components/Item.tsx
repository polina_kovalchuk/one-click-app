import React from 'react';
import styled from 'styled-components';
import { Status } from '../state/types';
import { FailureIcon, SuccessIcon } from '../icons';
import { Expand } from './Expand';

const getColor = (status?: Status) => {
  switch (status) {
    case Status.SUCCESSFUL:
      return '#397E33';
    case Status.FAILED:
    default:
      return '#CD3535';
  }
}

interface ContainerProps {
  status?: Status;
}

const Container = styled.div<ContainerProps>`
  font-family: Roboto Mono;
  font-style: normal;
  font-weight: normal;
  font-size: 22px;
  color: ${({ status }) => getColor(status)};
  display: flex;
  align-items: center;
  padding: 5px;
  line-height: 10px;
  
  svg {
    margin-right: 20px;
  }
  
  ${Expand} {
    margin-left: 20px;
  }
`;

interface ItemProps {
  status?: Status;
  label: string;
  onClick: () => void;
  isCollapsed: boolean;
}

export const Item: React.FC<ItemProps> = ({ status, label, onClick, isCollapsed }) => {
  return (
    <Container status={status}>
      {(() => {
        switch (status) {
          case Status.SUCCESSFUL:
            return <SuccessIcon />;
          case Status.FAILED:
          default:
            return <FailureIcon />;
        }
      })()}
      {label}
      <Expand isCollapsed={isCollapsed} onClick={onClick}/>
    </Container>
  );
}


