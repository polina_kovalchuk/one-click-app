import styled from 'styled-components';

export const MainActionContainer = styled.div`
  height: 120px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 70px;
`;