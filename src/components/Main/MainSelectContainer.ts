import styled from 'styled-components';

export const MainSelectContainer = styled.div`
  display: flex;
  height: 200px;
  flex-direction: column;
  justify-content: space-evenly;
`;