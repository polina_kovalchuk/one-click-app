import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  align-items: center;
`;

const Text = styled.span`
  font-family: Roboto;
  font-weight: 300;
  font-size: 36px;
  color: #295699;
  text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  margin-right: 20px;
`;

const Name = styled.span`
  font-family: Amatic SC;
  font-weight: bold;
  font-size: 48px;
  color: #295699;
  text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  margin-right: 20px;

`;

const Logo = styled.img`
  transform: rotate(-20deg);
`;

export const MainTitle = () => (
  <Container>
    <Text>Set up your analyzers in </Text>
    <Name>One Click</Name>
    <Logo src='/oneclick/logo.png' />
  </Container>
);
