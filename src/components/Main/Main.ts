import styled from 'styled-components';
import colors from '../../colors';
import { Page } from '../Page';

export const Main = styled(Page)`
  background: ${colors.backgroundNeutral};
  justify-content: space-evenly;
  align-items: center;
  padding-bottom: 50px;
`;
