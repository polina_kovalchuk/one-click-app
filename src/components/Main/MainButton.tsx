import styled from 'styled-components';
import React, { MouseEventHandler } from 'react';
import { StartIcon, Stop } from '../../icons';
import { Button } from '../Button';

interface ButtonProps {
  onClick?: MouseEventHandler<HTMLDivElement>;
}

const MainButton = styled(Button)`
  height: 64px;
  width: 230px;
  border: 5px solid white;
  border-radius: 15px;
  font-size: 33px;
  text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  justify-content: center;
  
  svg {
    margin-right: 20px;
  }
`;

const StopButtonContainer = styled(MainButton)`
  background: #C57867;
`;

export const StopButton: React.FC<ButtonProps> = ({ onClick }) => (
  <StopButtonContainer onClick={onClick} >
    <Stop />
    Stop
  </StopButtonContainer>
);

const StartButtonContainer = styled(MainButton)`
  background: #B2D9A8;
`;

export const StartButton: React.FC<ButtonProps> = ({ onClick }) => (
  <StartButtonContainer onClick={onClick} >
    <StartIcon />
    Start
  </StartButtonContainer>
);
