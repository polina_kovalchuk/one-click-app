import styled from 'styled-components';
import { TriangleIcon } from '../icons';

interface ExpandProps {
  isCollapsed: boolean;
  onClick: () => void;
}

export const Expand = styled(TriangleIcon)<ExpandProps>`
  cursor: pointer;
  transition: transform 0.5s;
  
  ${({isCollapsed}) => isCollapsed ? '' : 'transform: rotate(-180deg);' }
`;
