import React, { useEffect, useState } from 'react';
import styled, { keyframes } from 'styled-components';
import { RefreshIcon } from '../icons';

const Container = styled.div`
  color: #6D819E;
  font-size: 18px;
  font-weight: bold;
  font-family: Roboto Mono;
  display: flex;
  align-items: center;
`;

const rotateAnimation = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const Icon = styled(RefreshIcon)`
  cursor: pointer;
  animation: ${rotateAnimation} 2s ease-out 3s infinite;
`;

interface ProgressProps {
  refresh: () => void;
}

const messages = [
  'AIQ operations running',
  'Creating workspaces',
  'Registering applications',
  'Enabling applications',
  'Configuring Data Mapper',
  'Creating roles',
  'Configuring Workitem Type',
  'Creating offices and cabinets',
  'Creating Extraction Sets',
  'Setting up Workflows',
  'Updating Document Dictionary',
  'Configuring RPA Scripts',
  'Setting up DMS services',
  'Enabling queues'
];

let duplicate = 0;

export const Progress: React.FC<ProgressProps> = ({ refresh }) => {
  const [index, setIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      if (duplicate < messages.length - 1) {
        setIndex(duplicate + 1);
        duplicate++;
      } else {
        setIndex(0);
        duplicate = 0;
      }
    }, 3000);

    return () => clearInterval(interval);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Container>
      {messages[index] || ''}...<Icon onClick={refresh}/>
    </Container>
  );
}