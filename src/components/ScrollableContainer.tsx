import styled from 'styled-components';

const thumb = '#6b8b6047';
const track = 'rgba(229, 240, 232, 0.53)'

export const ScrollableContainer = styled.div`
  overflow: auto;
  scrollbar-width: thin;
  scrollbar-color: ${track};
  ::-webkit-scrollbar {
    width: 8px;
    height: 22px;
  }
  ::-webkit-scrollbar-track {
    background: ${track}; 
    border-radius: 10px;
  }
  ::-webkit-scrollbar-thumb {
    background: ${thumb};
    border-radius: 10px;
  } 
  ::-webkit-scrollbar-thumb:hover {
    background: ${thumb};
  } 
}
`;
