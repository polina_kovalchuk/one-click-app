export interface HistoryItem {
  siteName: string;
  encompassInstance: string;
  eventId: string;
}

export interface ReducerType {
  siteName: string | null;
  encompassInstance: string | null;
  activities: {[key: string]: Activity};
  actions: Action[];
  eventId: string | null;
  isRunning: boolean;
  history: { [key: string]: HistoryItem }
}

export interface State {
  reducer: ReducerType;
}

export enum Status {
  EMPTY,
  IN_PROGRESS,
  SUCCESSFUL,
  FAILED
}

export type ResponseStatuses = 'FAILURE' | 'SUCCESS' | 'PENDING';

export interface Activity {
  id: string;
  name: string;
  status: Status;
}

export interface Action {
  id: string;
  displayName: string;
  name: string;
  status: Status;
  description: string;
}
