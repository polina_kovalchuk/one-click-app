import { ReducerType, Status } from './types';
import { createSelector } from 'reselect';

export const getActivitiesMap = (state: ReducerType) => state.activities || {};
export const getActions = (state: ReducerType) => state.actions || [];
export const getEventId = (state: ReducerType) => state.eventId || '';
export const getIsRunning = (state: ReducerType) => state.isRunning;
export const getSiteName = (state: ReducerType) => state.siteName;
export const getEncompassId = (state: ReducerType) => state.encompassInstance;
export const getHistory = (state: ReducerType) => state.history;

export const getActivities = createSelector(
  [getActivitiesMap],
  (map) => Object.values(map)
);

export const getActivityIdByName = createSelector(
  [getActivitiesMap],
  (map) => (name: string) => (map[name] || {}).id
);

export const getEventIdFromHistory = createSelector(
  [getHistory, getSiteName, getEncompassId],
  (history, site, encompass) => (Object
    .values(history)
    .find((item) => item.siteName === site && item.encompassInstance) || {}).eventId
);

export const isConfigurationCompleted = createSelector(
  [getActivities],
  (activities = []) => activities.length && !activities
    .filter((item) => item.status === Status.IN_PROGRESS)
    .length
);
