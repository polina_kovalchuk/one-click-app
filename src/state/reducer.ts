import {
  actionsLoaded,
  activitiesLoaded,
  configurationStarted,
  setEncompassInstance,
  setSiteName,
  stopConfiguration
} from './actions';
import { PayloadAction } from '@reduxjs/toolkit';
import { ReducerType } from './types';
import { Reducer } from 'react';

const initialState: ReducerType = {
  siteName: null,
  encompassInstance: null,
  activities: {},
  actions: [],
  eventId: null,
  isRunning: false,
  history: {},
};

export const reducer: Reducer<any, any> = (state: ReducerType = initialState, { type, payload }: PayloadAction) => {
  switch (type) {
    case setSiteName.type:
      return {
        ...state,
        siteName: payload,
      }
    case setEncompassInstance.type:
      return {
        ...state,
        encompassInstance: payload,
      }
    case activitiesLoaded.type:
      return {
        ...state,
        activities: payload,
      }
    case actionsLoaded.type:
      return {
        ...state,
        actions: payload,
      }
    case configurationStarted.type:
      return {
        ...state,
        isRunning: true,
        eventId: payload,
        history: {
          ...state.history,
          [payload as unknown as string || '']: {
            siteName: state.siteName,
            eventId: payload,
            encompassInstance: state.encompassInstance,
          }
        }
      }
    case stopConfiguration.type:
      return {
        ...state,
        isRunning: false,
      }
    default:
      return state;
  }
};
