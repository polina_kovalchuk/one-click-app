import { Activity, Status } from './types';
import { fromCamelCase } from '../utils/utils';

export const parseActivities = (response: any = {}) => {
  const activities: Activity[] = (response.TrackingActivity || []).map(parseActivity);
  const map: { [key: string]: Activity} = {};
  activities.forEach((activity) => {
    map[activity.name] = activity;
  });
  return map;
}

const parseStatus = (response?: any): Status => {
  switch (response) {
    case ('FAILURE'):
      return Status.FAILED;
    case ('SUCCESS'):
      return Status.SUCCESSFUL;
    case ('PENDING'):
      return Status.IN_PROGRESS;
    default:
      return Status.EMPTY;
  }
};

const parseActivity = (response: any = {}) => ({
  name: response.name || '',
  id: response.activityId,
  status: parseStatus(response.status),
});

export const parseActions = (response: any = {}) => {
  return (response.TrackingAction || []).map(parseAction);
};

const parseAction = (response: any = {}) => ({
  name: response.name || '',
  displayName: fromCamelCase(response.name || ''),
  id: response.actionId,
  description: response.description || '',
  status: parseStatus(response.status),
});
