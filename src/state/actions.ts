import { createAction } from '@reduxjs/toolkit';
import { Action, Activity } from './types';

export const setSiteName = createAction<string | null>('SET_SITE_NAME');
export const setEncompassInstance = createAction<string | null>('SET_ENCOMPASS_ID');

export const startSetup = createAction('START_SETUP');
export const configurationStarted = createAction<string>('CONFIGURATION_STARTED');
export const stopConfiguration = createAction('STOP_CONFIGURATION');

export const loadActivities = createAction('LOAD_ACTIVITIES');
export const activitiesLoaded = createAction<{[key: string]: Activity}>('ACTIVITIES_LOADED');

export const loadActions = createAction('LOAD_ACTIONS');
export const actionsLoaded = createAction<Action[]>('ACTIONS_LOADED');
