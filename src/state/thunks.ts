import { actionsLoaded, activitiesLoaded, configurationStarted } from './actions';
import { parseActions, parseActivities } from './utils';
import { getActions, getActivities, runConfiguration as runConfigurationApi } from './api';
import { getActivityIdByName, getEventIdFromHistory } from './selectors';

export const fetchActivities = () => async (dispatch: any, getState: any) => {
  const eventId = getEventIdFromHistory(getState());
  if (eventId) {
    const { siteName } = getState();
    const { data } = await getActivities(eventId, siteName);
    // const { data } = await fetchMockActivities();
    dispatch(activitiesLoaded(parseActivities(data)));
  }
}

export const fetchActions = (name: string) => async (dispatch: any, getState: any) => {
  const eventId = getEventIdFromHistory(getState());
  const { siteName } = getState();
  const activityId = getActivityIdByName(getState())(name);
  const { data } = await getActions(eventId, activityId, siteName);
  // const { data } = await fetchMockActions();
  const actions = parseActions(data);
  dispatch(actionsLoaded(actions));
}

export const runConfiguration = () => async (dispatch: any, getState: any) => {
  const { siteName, encompassInstance } = getState();
  const { data } = await runConfigurationApi(siteName, encompassInstance);
  const eventId = data?.configureSiteResponse?.eventId || null;
  // const eventId = 'CONFIG_A2E0E0FF-0D66-3CC2-B74D-E325BE3CF994';
  dispatch(configurationStarted(eventId));
};
