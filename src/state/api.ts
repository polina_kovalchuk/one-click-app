import { fetchMockActions } from '../mocks/get-actions';
import { fetchMockActivities } from '../mocks/get-activities';
import { httpClient } from '../utils/http';

export const getActivities = (
  eventId = 'CONFIG_8036E503-AC30-6B23-25F1-58F9AD3342A9',
  siteName = 'lis2-migration1.acceptance1-capsilondev.net',
) => httpClient.get(
  `/configurations/getActivities?siteAddress=${siteName}&eventId=${eventId}`
).catch(() => fetchMockActivities());

export const getActions = (
  eventId = 'CONFIG_8036E503-AC30-6B23-25F1-58F9AD3342A9',
  activityId = 'CBE3BC04-2ADB-87F7-9CC3-31EBC4D041B5',
  siteName = 'lis2-migration1.acceptance1-capsilondev.net',
) => httpClient.get(
  `/configurations/getActions?siteAddress=${siteName}&eventId=${eventId}&activityId=${activityId}`
).catch(() => fetchMockActions());

export const runConfiguration = (
  siteName: string = 'lis2-migration1.acceptance1-capsilondev.net',
  encompassInstanceId: string = 'TEBE11212339',
) => httpClient.post(
  `/configurations/configureAnalyserForSite?siteAddress=${siteName}&encompassInstanceId=${encompassInstanceId}`,
  {},
  { headers: { Accept: 'application/json' } },
).then(data => {
  return data;
})
